#include "./ps4.h"

ps4Data::ps4Data(Serial& serial, GamepadValue& gamepad) : _ps4(serial), _gamepad(gamepad){
    serial.attach(callback(this, &ps4Data::on_received));
}

void ps4Data::on_received(){
    if(_ps4.getc() == 0x15){
      _receive  = true;
        for(int a = 0;; a++){
            data[a] = (int)_ps4.getc();
            if(a == 0){
                checkSum = data[0];
            }
            else if(data[a] == checkSum && a == 7){
                break;
            }
        }
        _gamepad.x = (int)data[1] >> 3& 0x1;
        _gamepad.a = (int)data[1] >> 2& 0x01;
        _gamepad.b = (int)data[1] >> 1 & 0x001;
        _gamepad.y = (int)data[1]& 0x0001;
        _gamepad.up = (int)data[2] >> 3& 0x1;
        _gamepad.right = (int)data[2] >> 2 & 0x01;
        _gamepad.down  = (int)data[2]  >> 1 & 0x001;
        _gamepad.left = (int)data[2]& 0x0001;
        _gamepad.l1  = (int)data[3] >> 3& 0x1;
        _gamepad.r1 = (int)data[3] >> 2 & 0x01;
        _gamepad.l3  = (int)data[3]  >> 1 & 0x001;
        _gamepad.r3 = (int)data[3] & 0x0001;
        _gamepad.psTouchPad  = (int)data[4] >> 2 & 0x1;
        _gamepad.l2 = (int)data[4] >> 1 & 0x01;
        _gamepad.r2  = (int)data[4] & 0x001;

        _gamepad.left_y = (int)data[5];
        if(_gamepad.left_y > 138){
            _gamepad.left_y -= 128;
        }
        else if(_gamepad.left_y < 118){
            _gamepad.left_y -= 127;
        }
        else{
            _gamepad.left_y = 0;
        }

        _gamepad.right_y = (int)data[6];
        if(_gamepad.right_y > 138){
            _gamepad.right_y -= 128;
        }
        else if(_gamepad.right_y < 118){
            _gamepad.right_y -= 127;
        }
        else{
            _gamepad.right_y = 0;
        }
    }
}
