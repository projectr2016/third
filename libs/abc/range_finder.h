/** 距離計の基底クラス群
 *
 * @file
 */

#ifndef __ABC_RANGE_FINDER_H__
#define __ABC_RANGE_FINDER_H__


/** 距離計のための基底クラス
 */
class RangeFinder {
public:
	/** 距離を取得する
	 *
	 * @return  障害物までの距離[mm]
	 */
	virtual float get() = 0;
};

#endif
