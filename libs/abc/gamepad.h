/** ゲームパッドを扱うためのクラスの基底と、値の定義
 *
 * @file
 */

#ifndef __GAMEPAD_H__
#define __GAMEPAD_H__


/** ゲームパッドの入力を表わす構造体
 */
struct GamepadValue {
    float left_y;   ///< 左ジョイスティックの前後(-1.0(下) ~ +1.0(上))
    float left_x;   ///< 左ジョイスティックの左右(-1.0(左) ~ +1.0(右))
    float right_y;  ///< 右ジョイスティックの前後(-1.0(下) ~ +1.0(上))
    float right_x;  ///< 右ジョイスティックの左右(-1.0(左) ~ +1.0(右))

    bool l1;  ///< L1ボタンが押されているかどうか
    bool l2;  ///< L2ボタンが押されているかどうか
    bool r1;  ///< R1ボタンが押されているかどうか
    bool r2;  ///< R2ボタンが押されているかどうか
    bool r3;
    bool l3;

    bool up;     ///< 上ボタンが押されているかどうか
    bool down;   ///< 下ボタンが押されているかどうか
    bool left;   ///< 左ボタンが押されているかどうか
    bool right;  ///< 右ボタンが押されているかどうか
    bool psTouchPad;
    bool psOption;
    bool psShare;

    bool a;  ///< Aボタン（丸ボタン）が押されているかどうか
    bool b;  ///< Bボタン（バツボタン）が押されているかどうか
    bool x;  ///< Xボタン（三角ボタン）が押されているかどうか
    bool y;  ///< Yボタン（四角ボタン）が押されているかどうか
};


/** ゲームパッドからの入力を読むためのクラスの基底
 */
class Gamepad {
public:
    /** 現在の入力を取得する。
     */
    virtual GamepadValue get() = 0;
};

#endif
