/** エンコーダーの基底クラス群
 *
 * @file
 */

#ifndef __ABC_ENCODER_H__
#define __ABC_ENCODER_H__


/** エンコーダーのための既定クラス
 */
class Encoder {
public:
	/** 値をリセットする
	 */
	virtual void reset() = 0;

	/** 現在までに送出されたパルス数を取得する
	 *
	 * 値はクラスが作られてから、もしくは最後にリセットされてからのもの。
	 *
	 * @return  今までのパルス数
	 */
	virtual int getPulse() = 0;

	/** 現在までに回転した回数を取得する
	 *
	 * 値はクラスが作られてから、もしくは最後にリセットされてからのもの。
	 *
	 * @return  今まで回転した回数
	 */
	virtual float get() = 0;
};


/** アブソリュートエンコーダーのための基底クラス
 */
class AbsoluteEncoder {
public:
    /** 現在の角度を取得する
     */
    virtual int get() = 0;
};

#endif
