/** モーターの基底クラス群
 *
 * @file
 */

#ifndef __ABC_MOTOR_H__
#define __ABC_MOTOR_H__


/** 基本的なモーターのための基底クラス
 *
 * 速度制御などは行なわず、全力との割合だけで指定する。
 * 現在の値を取得することは出来ない。
 *
 * 値域を-1.0から1.0にすることで、モータードライバの仕様やモーターの性能などを考慮せずにプログラムを書けるようにすることが目的。
 * また、同一の基底クラスを持っていれば、モータードライバが変更になっても宣言の部分を書き換えるだけで済む。
 */
class Motor {
public:
    /** モーターへの出力レベルを設定する
     *
     * @param level  設定する出力レベル。1.0なら全力で正転、0.0なら停止、-1.0なら全力で逆転。
     */
    virtual void set(float level) = 0;
};


/** モーターを速度制御するための基底クラス
 *
 * 入出力は角速度（ラジアン毎秒）で行なう。
 */
class ManagedMotor {
public:
    /** 目標角速度を設定する
     *
     * @param speed  目標角速度[rad/sec]
     */
    virtual void set(float speed) = 0;

    /** 現在の角速度を取得する
     *
     * @return  現在の角速度[rad/sec]
     */
    virtual float get() = 0;
};


/** モーターを位置制御するための基底クラス
 *
 * 入出力はミリメートル単位で行なう。
 */
class PositionMotor {
public:
    /** 現在位置を上書き設定する
     *
     * @param position  新しく設定する現在位置の値[mm]。省略した場合は0になる。
     */
    virtual void reset(float position=0) = 0;

    /** 目標位置を設定する
     *
     * @param position  目標位置[mm]
     */
    virtual void set(float position) = 0;

    /** 現在の位置を取得する
     *
     * @return  現在の位置[mm]
     */
    virtual float get() = 0;
};

#endif
