/** シリンダー制御の基底クラス群
 *
 * @file
 */

#ifndef __ABC_CYLINDER_H__
#define __ABC_CYLINDER_H__


/** シンプルなシリンダー制御のための基底クラス
 *
 * 速度を制御せず、開放と閉鎖のみを行なう。
 */
class SimpleCylinder {
public:
    /** 伸ばす
     */
    virtual void extend() = 0;

    /** 縮める
     */
    virtual void shorten() = 0;

    /** 現在シリンダーが伸びているか確認する
     *
     * ソフトウェア上で伸びていることになっているのか、それともセンサで本当に伸びているかを確認しているのかは子クラスの実装による。
     * 各クラスの仕様を確認してから使うこと。
     */
    virtual bool is_extended() = 0;
};

#endif
