/** サーボモーターの基底クラス群
 *
 * @file
 */

#ifndef __ABC_SERVO_H__
#define __ABC_SERVO_H__


/** サーボモーターのための基底クラス
 */
class Servo {
public:
    /** 目標角度を設定する
     *
     * @param rad  目標角度[radians]
     */
    virtual void set(float rad) = 0;

    /** 角度を取得する
     *
     * @attention 現在の角度を取得出来るのか、あるいは設定されている目標角度が取得出来るのかはサーボによって違う。注意して使うこと。
     *
     * @return  現在の角度、もしくは目標角度[radians]
     */
    virtual float get() = 0;
};

#endif
