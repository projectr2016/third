#include "RoboClaw.h"
#include <stdarg.h>

#define MAXTRY 2
#define PI 3.14159265
#define BAUDRATE 115200
#define SetDWORDval(arg) (uint8_t)(arg>>24),(uint8_t)(arg>>16),(uint8_t)(arg>>8),(uint8_t)arg
#define SetWORDval(arg) (uint8_t)(arg>>8),(uint8_t)arg

RoboClawSpeed::RoboClawSpeed(PinName tx, PinName rx, int adderss, int motor_number, int ppr):RoboClaw(tx, rx, adderss,  motor_number, ppr){
}

void RoboClawSpeed::set(float speed){
    setAngleSpeed(speed);
}

float RoboClawSpeed::get(){
    return getAngleSpeed();
}


RoboClaw::RoboClaw(PinName tx, PinName rx, int adderss, int motor_number, int ppr):RawRoboClaw(BAUDRATE, tx, rx){
    _motor_number = motor_number;
    _address = adderss;
    _ppr = ppr;
}

void RoboClaw::setPWM(int duty){
	switch (_motor_number){
	case 1:
	    if(duty > 0){
	        ForwardM1(_address, duty);
	    }else {
	        BackwardM1(_address, -duty);
	    }
	    break;
	case 2:
		if(duty > 0){
	        ForwardM2(_address, duty);
	    }else {
	        BackwardM2(_address, -duty);
	    }
	    break;
	}
}


void RoboClaw::setSpeed(int speed){
	switch (_motor_number){
	case 1:
	    SpeedM1(_address,speed);    
	    break;
	case 2:
		SpeedM2(_address,speed);    
	    break;
	}
}

void RoboClaw::setAngleSpeed(double angle_speed){
    int speed = angle_speed * _ppr * 4;
    switch (_motor_number){
   	case 1:
   	    SpeedM1(_address,speed);    
   	    break;
   	case 2:
   		SpeedM2(_address,speed);    
   	    break;
   	}
}

int RoboClaw::getPulse(){
	int pulse = 0;
	switch (_motor_number){
	case 1:
	   	pulse = ReadEncM1(_address);
	    break;
	case 2:
		pulse = ReadEncM2(_address);  
	    break;
	}
    return pulse;
}

double RoboClaw::getAngle(){
    return (2.0 * PI /(_ppr*4)) * getPulse();
}

int RoboClaw::getSpeed(){
	int speed = 0;
	switch (_motor_number){
	case 1:
	    speed = ReadSpeedM1(_address);    
	    break;
	case 2:
		speed = ReadSpeedM2(_address);    
	    break;
	}
    return speed;;
}

double RoboClaw::getAngleSpeed(){
    return getSpeed() / _ppr*4;
}

   
RawRoboClaw::RawRoboClaw(int baudrate, PinName tx, PinName rx): _roboclaw(tx,rx){
    _roboclaw.baud(baudrate);
}

void RawRoboClaw::crc_clear(){
    crc = 0;
}

void RawRoboClaw::crc_update (uint8_t data){
    int i;
    crc = crc ^ ((uint16_t)data << 8);
    for (i=0; i<8; i++) {
        if (crc & 0x8000)
            crc = (crc << 1) ^ 0x1021;
        else
            crc <<= 1;
    }
}

uint16_t RawRoboClaw::crc_get(){
    return crc;
}

void RawRoboClaw::write_n(uint8_t cnt, ... ){
    uint8_t retry = MAXTRY;
    do {
        crc_clear();
        va_list marker;
        va_start( marker, cnt );
        for(uint8_t index=0; index<cnt; index++) {
            uint8_t data = va_arg(marker, unsigned int);
            crc_update(data);
            _roboclaw.putc(data);
        }
        va_end( marker );
        uint16_t crc = crc_get();
        _roboclaw.putc(crc>>8);
        _roboclaw.putc(crc);
    } while(retry--);
}

void RawRoboClaw::write_(uint8_t address, uint8_t command, uint8_t data, bool reading, bool crcon){
    _roboclaw.putc(address);
    _roboclaw.putc(command);

    if(reading == false) {
        if(crcon == true) {
            uint8_t packet[2] = {address, command};
            uint16_t checksum = crc16(packet,2);
            _roboclaw.putc(checksum>>8);
            _roboclaw.putc(checksum);
        } else {
            uint8_t packet[3] = {address, command, data};
            uint16_t checksum = crc16(packet,3);
            _roboclaw.putc(data);
            _roboclaw.putc(checksum>>8);
            _roboclaw.putc(checksum);
        }
    }
}

uint16_t RawRoboClaw::crc16(uint8_t *packet, int nBytes){
    uint16_t crc_ = 0;
    for (int byte = 0; byte < nBytes; byte++) {
        crc_ = crc_ ^ ((uint16_t)packet[byte] << 8);
        for (uint8_t bit = 0; bit < 8; bit++) {
            if (crc_ & 0x8000) {
                crc_ = (crc_ << 1) ^ 0x1021;
            } else {
                crc_ = crc_ << 1;
            }
        }
    }
    return crc_;
}

uint8_t RawRoboClaw::read_(void){
    return(_roboclaw.getc());
}

void RawRoboClaw::ForwardM1(uint8_t address, int speed){
    write_(address,M1FORWARD,speed,false,false);
}

void RawRoboClaw::BackwardM1(uint8_t address, int speed){
    write_(address,M1BACKWARD,speed,false,false);
}

void RawRoboClaw::ForwardM2(uint8_t address, int speed){
    write_(address,M2FORWARD,speed,false,false);
}

void RawRoboClaw::BackwardM2(uint8_t address, int speed){
    write_(address,M2BACKWARD,speed,false,false);
}

void RawRoboClaw::Forward(uint8_t address, int speed){
    write_(address,MIXEDFORWARD,speed,false,false);
}

void RawRoboClaw::Backward(uint8_t address, int speed){
    write_(address,MIXEDBACKWARD,speed,false,false);
}

void RawRoboClaw::ReadFirm(uint8_t address){
    write_(address,GETVERSION,0x00,true,false);
}

int32_t RawRoboClaw::ReadData(){
	int32_t data;
	uint16_t read_byte[7];
    
	read_byte[0] = (uint16_t)_roboclaw.getc();
    read_byte[1] = (uint16_t)_roboclaw.getc();
    read_byte[2] = (uint16_t)_roboclaw.getc();
    read_byte[3] = (uint16_t)_roboclaw.getc();
    read_byte[4] = (uint16_t)_roboclaw.getc();
    read_byte[5] = (uint16_t)_roboclaw.getc();
    read_byte[6] = (uint16_t)_roboclaw.getc();

	data = read_byte[1]<<24;
	data |= read_byte[2]<<16;
	data |= read_byte[3]<<8;
	data |= read_byte[4];

	return data;
}

int32_t RawRoboClaw::ReadEncM1(uint8_t address){
    write_n(2,address,GETM1ENC);
    return ReadData();
}

int32_t RawRoboClaw::ReadEncM2(uint8_t address){
    write_(address,GETM2ENC,0x00, true,false);
    return ReadData();
}

int32_t RawRoboClaw::ReadSpeedM1(uint8_t address){
    write_n(2,address,GETM1SPEED);
    return ReadData();
}

int32_t RawRoboClaw::ReadSpeedM2(uint8_t address){
    write_n(2,address,GETM2SPEED);
    return ReadData();
}

void RawRoboClaw::ResetEnc(uint8_t address){
    write_n(2,address,RESETENC);
}

void RawRoboClaw::SpeedM1(uint8_t address, int32_t speed){
    write_n(6,address,M1SPEED,SetDWORDval(speed));
}

void RawRoboClaw::SpeedM2(uint8_t address, int32_t speed){
    write_n(6,address,M2SPEED,SetDWORDval(speed));
}

void RawRoboClaw::SpeedAccelM1(uint8_t address, int32_t accel, int32_t speed){
    write_n(10,address,M1SPEEDACCEL,SetDWORDval(accel),SetDWORDval(speed));
}

void RawRoboClaw::SpeedAccelM2(uint8_t address, int32_t accel, int32_t speed){
    write_n(10,address,M2SPEEDACCEL,SetDWORDval(accel),SetDWORDval(speed));
}

void RawRoboClaw::SpeedDistanceM1(uint8_t address, int32_t speed, uint32_t distance, uint8_t buffer){
    write_n(11,address,M1SPEEDDIST,SetDWORDval(speed),SetDWORDval(distance),buffer);
}

void RawRoboClaw::SpeedDistanceM2(uint8_t address, int32_t speed, uint32_t distance, uint8_t buffer){
    write_n(11,address,M2SPEEDDIST,SetDWORDval(speed),SetDWORDval(distance),buffer);
}

void RawRoboClaw::SpeedAccelDistanceM1(uint8_t address, int32_t accel, int32_t speed, uint32_t distance, uint8_t buffer){
    write_n(15,address,M1SPEEDACCELDIST,SetDWORDval(accel),SetDWORDval(speed),SetDWORDval(distance),buffer);
}

void RawRoboClaw::SpeedAccelDistanceM2(uint8_t address, int32_t accel, int32_t speed, uint32_t distance, uint8_t buffer){
    write_n(15,address,M2SPEEDACCELDIST,SetDWORDval(accel),SetDWORDval(speed),SetDWORDval(distance),buffer);
}

void RawRoboClaw::SpeedAccelDeccelPositionM1(uint8_t address, uint32_t accel, int32_t speed, uint32_t deccel, int32_t position, uint8_t flag){
    write_n(19,address,M1SPEEDACCELDECCELPOS,SetDWORDval(accel),SetDWORDval(speed),SetDWORDval(deccel),SetDWORDval(position),flag);
}

void RawRoboClaw::SpeedAccelDeccelPositionM2(uint8_t address, uint32_t accel, int32_t speed, uint32_t deccel, int32_t position, uint8_t flag){
    write_n(19,address,M2SPEEDACCELDECCELPOS,SetDWORDval(accel),SetDWORDval(speed),SetDWORDval(deccel),SetDWORDval(position),flag);
}

void RawRoboClaw::SpeedAccelDeccelPositionM1M2(uint8_t address,uint32_t accel1,uint32_t speed1,uint32_t deccel1, int32_t position1,uint32_t accel2,uint32_t speed2,uint32_t deccel2, int32_t position2,uint8_t flag){
    write_n(35,address,MIXEDSPEEDACCELDECCELPOS,SetDWORDval(accel1),SetDWORDval(speed1),SetDWORDval(deccel1),SetDWORDval(position1),SetDWORDval(accel2),SetDWORDval(speed2),SetDWORDval(deccel2),SetDWORDval(position2),flag);
}


