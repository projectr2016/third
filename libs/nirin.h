#ifndef __nirin_h__
#define __nirin_h__

#include "mbed.h"
#include "./RoboClaw.h"



class nirin{
private:
    Ticker ticker;
    RoboClaw& _rightMotor;
    RoboClaw& _leftMotor;
public:
    nirin(RoboClaw& rightRoboclaw, RoboClaw& leftRoboclaw);
    void control(float x_speed, float y_speed);
    void first_order_lag(void);
    void setSpeed(float x_speed, float y_speed);
};

#endif
