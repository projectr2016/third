#include "./difenceAngle.h"

#define K0 1.5
#define dt 0.03
#define T0 0.12

float turn_f = 0, elevation_f = 0;
float target_turn_speed = 0, target_elevation_speed = 0;

difenceAngle::difenceAngle(RoboClaw& turnMotor, RoboClaw& elevationMotor, QEI& qei) : _turnMotor(turnMotor),  _elevationMotor(elevationMotor), _qei(qei){
    ticker.attach(this,&difenceAngle::first_order_lag, dt);
}

void difenceAngle::control(float turning_speed, float elevation_angle){
    target_turn_speed = turning_speed;
    target_elevation_speed = elevation_angle;
}

void difenceAngle::first_order_lag(void){
    turn_f += (K0 * target_turn_speed - turn_f) * dt / T0;
    elevation_f += (K0 * target_elevation_speed - elevation_f) * dt / T0;

    _pulse = _qei.getAngle();
    //負の限界値に到達した時に，負方向への速度をブロックする
    if(_pulse <= 80){
        if(turn_f < 0){
            turn_f = 0;
        }
    }
    else if(_pulse >= 80){
        if(turn_f > 0){
            turn_f = 0;
        }
    }

    setSpeed(turn_f, elevation_f);
}

void difenceAngle::setSpeed(float turning_speed, float elevation_angle_speed){
    _turnMotor.setPWM(turning_speed);
    _elevationMotor.setPWM(elevation_angle_speed);
}
