#ifndef ROBOCLAW_H
#define ROBOCLAW_H
#include "mbed.h"
#include "abc/motor.h"

/*サンプルコード
#include "mbed.h"
#include "RoboClaw.h"
//ボーレート9600kbps,txピンp9,rxピンp10,アドレス128,エンコーダから1回転に送出されるパルスが2048パルスの場合
RoboClaw roboclaw(9600, p9, p10, 128, 2048);

int main(void)
{
    //PWM制御の場合(-127 ~ 127の間で指定する　０で停止)
    roboclaw.setPWMM1(127);
    roboclaw.setPWMM2(-127);
    //速度制御の場合（目標値 1.0 rad/s）
    roboclaw.setAngleSpeedM1(1.0);
    roboclaw.setAngleSpeedM2(1.0);
    //現在のエンコーダの値を取得する
    int pulseM1 = roboclaw.getPulseM1();
    int pulseM2 = roboclaw.getPulseM2();
    //角度[rad]を取得する
    double radM1 = roboclaw.getAngleM1();
    double radM2 = roboclaw.getAngleM2();

}
*/

class RawRoboClaw
{
protected:
	enum {
		M1FORWARD = 0,
        M1BACKWARD = 1,
        SETMINMB = 2,
        SETMAXMB = 3,
        M2FORWARD = 4,
        M2BACKWARD = 5,
        M17BIT = 6,
        M27BIT = 7,
        MIXEDFORWARD = 8,
        MIXEDBACKWARD = 9,
        MIXEDRIGHT = 10,
        MIXEDLEFT = 11,
        MIXEDFB = 12,
        MIXEDLR = 13,
        GETM1ENC = 16,
        GETM2ENC = 17,
        GETM1SPEED = 18,
        GETM2SPEED = 19,
        RESETENC = 20,
        GETVERSION = 21,
        GETMBATT = 24,
        GETLBATT = 25,
        SETMINLB = 26,
        SETMAXLB = 27,
        SETM1PID = 28,
        SETM2PID = 29,
        GETM1ISPEED = 30,
        GETM2ISPEED = 31,
        M1DUTY = 32,
        M2DUTY = 33,
        MIXEDDUTY = 34,
        M1SPEED = 35,
        M2SPEED = 36,
        MIXEDSPEED = 37,
        M1SPEEDACCEL = 38,
        M2SPEEDACCEL = 39,
        MIXEDSPEEDACCEL = 40,
        M1SPEEDDIST = 41,
        M2SPEEDDIST = 42,
        MIXEDSPEEDDIST = 43,
        M1SPEEDACCELDIST = 44,
        M2SPEEDACCELDIST = 45,
        MIXEDSPEEDACCELDIST = 46,
        GETBUFFERS = 47,
        GETCURRENTS = 49,
        MIXEDSPEED2ACCEL = 50,
        MIXEDSPEED2ACCELDIST = 51,
        M1DUTYACCEL = 52,
        M2DUTYACCEL = 53,
        MIXEDDUTYACCEL = 54,
        READM1PID = 55,
        READM2PID = 56,
        SETMAINVOLTAGES = 57,
        SETLOGICVOLTAGES = 58,
        GETMINMAXMAINVOLTAGES = 59,
        GETMINMAXLOGICVOLTAGES = 60,
        SETM1POSPID = 61,
        SETM2POSPID = 62,
        READM1POSPID = 63,
        READM2POSPID = 64,
        M1SPEEDACCELDECCELPOS = 65,
        M2SPEEDACCELDECCELPOS = 66,
        MIXEDSPEEDACCELDECCELPOS = 67,
        GETTEMP = 82,
        GETERROR = 90,
        GETENCODERMODE = 91,
        SETM1ENCODERMODE = 92,
        SETM2ENCODERMODE = 93,
        WRITENVM = 94
	};
    /** Create RawRawRoboClaw instance
    */
    RawRoboClaw(int baudrate, PinName tx, PinName rx);

    /** Forward and Backward functions
    * @param address address of the device
    * @param speed speed of the motor (between 0 and 127)
    * @note Forward and Backward functions
    */
    void ForwardM1(uint8_t address, int speed);
    void BackwardM1(uint8_t address, int speed);
    void ForwardM2(uint8_t address, int speed);
    void BackwardM2(uint8_t address, int speed);

    /** Forward and Backward functions
    * @param address address of the device
    * @param speed speed of the motor (between 0 and 127)
    * @note Forward and Backward functions, it turns the two motors
    */
    void Forward(uint8_t address, int speed);
    void Backward(uint8_t address, int speed);

    /** Read the Firmware
    * @param address address of the device
    */
    void ReadFirm(uint8_t address);

    /** Read encoder and speed of M1 or M2
    * @param address address of the device
    * @note Read encoder in ticks
    * @note Read speed in ticks per second
    */
    int32_t ReadData();
    int32_t ReadEncM1(uint8_t address);
    int32_t ReadEncM2(uint8_t address);
    int32_t ReadSpeedM1(uint8_t address);
    int32_t ReadSpeedM2(uint8_t address);

    /** Set both encoders to zero
    * @param address address of the device
    */
    void ResetEnc(uint8_t address);

    /** Set speed of Motor with different parameter (only in ticks)
    * @param address address of the device
    * @note Set the Speed
    * @note Set the Speed and Accel
    * @note Set the Speed and Distance
    * @note Set the Speed, Accel and Distance
    * @note Set the Speed, Accel, Decceleration and Position
    */
    void SpeedM1(uint8_t address, int32_t speed);
    void SpeedM2(uint8_t address, int32_t speed);
    void SpeedAccelM1(uint8_t address, int32_t accel, int32_t speed);
    void SpeedAccelM2(uint8_t address, int32_t accel, int32_t speed);
    void SpeedDistanceM1(uint8_t address, int32_t speed, uint32_t distance, uint8_t buffer);
    void SpeedDistanceM2(uint8_t address, int32_t speed, uint32_t distance, uint8_t buffer);
    void SpeedAccelDistanceM1(uint8_t address, int32_t accel, int32_t speed, uint32_t distance, uint8_t buffer);
    void SpeedAccelDistanceM2(uint8_t address, int32_t accel, int32_t speed, uint32_t distance, uint8_t buffer);
    void SpeedAccelDeccelPositionM1(uint8_t address, uint32_t accel, int32_t speed, uint32_t deccel, int32_t position, uint8_t flag);
    void SpeedAccelDeccelPositionM2(uint8_t address, uint32_t accel, int32_t speed, uint32_t deccel, int32_t position, uint8_t flag);
    void SpeedAccelDeccelPositionM1M2(uint8_t address,uint32_t accel1,uint32_t speed1,uint32_t deccel1, int32_t position1,uint32_t accel2,uint32_t speed2,uint32_t deccel2, int32_t position2,uint8_t flag);
private:
    Serial _roboclaw;
    uint16_t crc;
    void crc_clear();
    void crc_update(uint8_t data);
    uint16_t crc_get();

    void write_n(uint8_t cnt, ...);
    void write_(uint8_t address, uint8_t command, uint8_t data, bool reading, bool crcon);

    uint16_t crc16(uint8_t *packet, int nBytes);
    uint8_t read_(void);
};

class RoboClaw: private RawRoboClaw
{
public:
    /** RoboClawのインスタンス
    * @param baudrate ボーレート
    * @param tx TXピン番号
    * @param rx RXピン番号
    * @param address 通信するRoboClawのアドレス
    * @param motor_number 1 = M1のモータの制御 2 = M2のモータの制御
    * @param ppr エンコーダが1回転するごとに送出されるパルスの数（1逓倍）
    */
    RoboClaw(PinName tx, PinName rx, int address,int motor_number , int ppr);

    /** モータをPWMで回転させる
    * @param duty duty比(-127 ~ 127) 0で止まる
    */
    void setPWM(int duty);

    /** モータの目標速度を指定する
    * @param speed 1秒間に送出されるパルスを目標にして回転させる
    */
    void setSpeed(int speed);

    /** モータの目標速度を角速度で指定する
    * @param angle_speed 目標角度速[rad/s]
    */
    void setAngleSpeed(double angle_speed);

    /** 現在までエンコーダが送出したパルスを取得
    * @note パルスを取得
    */
    int getPulse();

    /**　角度を取得する
    * @note ラジアンで角度を取得する
    */
    double getAngle();

    /** 速度を取得する
    * @note パルス/秒で速度を取得
    */
    int getSpeed();

    /** 現在の角速度を取得する
    * @note rad/sで速度を取得
    */
    double getAngleSpeed();

private:
    /** 初期化する
    */
    int _address;
    int _ppr;
    int _motor_number;
};

class RoboClawSpeed:public ManagedMotor,public RoboClaw{
public:
    RoboClawSpeed(PinName tx, PinName rx, int address,int motor_number , int ppr);
    /** 角速度を指定する
     * @param speed  指定したい角速度[rad/s]
     */
    virtual void set(float speed);
	/** 現在の角速度を取得する
     * エンコーダーからの実際の値が返却される
     * @return  現在の実際の角速度[rad/s]
     */
    virtual float get();
};
#endif
