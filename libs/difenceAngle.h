#ifndef __difenceAngle_h__
#define __difenceAngle_h__

#include "mbed.h"
#include "./RoboClaw.h"
#include "./QEI.h"

class difenceAngle{
private:
    int _pulse;
    QEI& _qei;
    Ticker ticker;
    RoboClaw& _turnMotor;
    RoboClaw& _elevationMotor;
public:
    difenceAngle(RoboClaw& turnMotor, RoboClaw& elevationMotor, QEI& qei);
    void control(float turning_speed, float elevation_angle);
    void first_order_lag(void);
    void setSpeed(float turning_speed, float elevation_angle_speed);
};

#endif
