#ifndef __ps4_h__
#define __ps4_h__

#include "abc/gamepad.h"
#include "mbed.h"

class ps4Data{
private:
    int checkSum;
    int data[15];
    Serial& _ps4;
    GamepadValue& _gamepad;
public:
    bool _receive = false;
    ps4Data(Serial& serial, GamepadValue& gamepad);

    void on_received();
};

#endif
