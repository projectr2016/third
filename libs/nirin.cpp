#include "./nirin.h"

#define K0 1.5
#define dt 0.02
#define T0 0.03

float x_f = 0, y_f = 0;
float target_x_speed = 0, target_y_speed = 0;

nirin::nirin(RoboClaw& rightRoboclaw, RoboClaw& leftRoboclaw) : _rightMotor(rightRoboclaw),  _leftMotor(leftRoboclaw){
    ticker.attach(this,&nirin::first_order_lag, dt);
}

void nirin::control(float x_speed, float y_speed){
    target_x_speed = x_speed;
    target_y_speed = y_speed;
}

void nirin::first_order_lag(void){
    x_f += (K0 * target_x_speed - x_f) * dt / T0;
    y_f += (K0 * target_y_speed - y_f) * dt / T0;

    setSpeed(x_f, y_f);
}

void nirin::setSpeed(float x_speed, float y_speed){
    // printf("%lf\t%lf\r\n", x_speed, y_speed);
    _rightMotor.setPWM(x_speed);
    _leftMotor.setPWM(y_speed);
}
