#ifndef __register_h__
#define __resister_h__


#include<stdio.h>
#include"mbed.h"
#include "libs/ps4.h"
#include "libs/abc/gamepad.h"
#include "libs/RoboClaw.h"
#include "libs/difenceAngle.h"
#include "libs/nirin.h"
#include "libs/QEI.h"

Timer t;
QEI qei(p17, p18, NC, 2048, &t);

Serial serial(p13, p14, 115200);
Serial pc(USBTX, USBRX, 115200);

RoboClaw underBodyRightMotor(p9, NC, 128, 1, 2048);
RoboClaw underBodyLeftMotor(p9, NC, 128, 2, 2048);

nirin underBody(underBodyRightMotor, underBodyLeftMotor);


RoboClaw elevationMotor(p9, NC, 129, 1, 2048);
RoboClaw turningMotor(p9, NC, 129, 2, 2048);

difenceAngle difence(turningMotor, elevationMotor, qei);


GamepadValue _gamepad;
ps4Data ps4(serial, _gamepad);



PwmOut cobra(p21);
PwmOut cobra2(p22);
DigitalOut mochiageValve(p24);
DigitalOut oshidashiValve(p23);
DigitalOut ben(p26);
DigitalOut beben(p25);

//これバカすぎ
DigitalOut uuat(p28);
DigitalOut uuat2(p27);
DigitalOut led(LED1);
DigitalOut led2(LED2);
DigitalOut led3(LED3);
DigitalOut led4(LED4);

#endif
